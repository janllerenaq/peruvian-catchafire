from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):

    ONG_MEMBER = 'ong-member'
    DONOR_INSTITUTION = 'donor'

    USER_TYPES = (
        (ONG_MEMBER, ONG_MEMBER),
        (DONOR_INSTITUTION, DONOR_INSTITUTION)
    )

    user_type = models.CharField(
        max_length=16, choices=USER_TYPES, default=DONOR_INSTITUTION
    )
    address = models.TextField()
    image = models.ImageField()
