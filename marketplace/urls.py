from django.urls import path

from . import views


app_name = 'marketplace'

urlpatterns = [
    path('products/', views.list_products, name='products-list'),
    path('products/add/', views.create_product, name='products-add'),
    path('', views.list_offers, name='offers-list'),
    path('offers/add/', views.create_offer, name='offers-add'),
    path('offers/<int:pk>/', views.offer_detail, name='offer-detail'),
    path('demands/', views.list_demand, name='demands-list'),
]
