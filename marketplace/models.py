from django.db import models
from django.conf import settings


class Product(models.Model):
    FOOD = 'food'
    CLOTHES = 'clothes'

    CATEGORIES = (
        (FOOD, FOOD),
        (CLOTHES, CLOTHES)
    )
    name = models.CharField(max_length=64)
    category = models.CharField(max_length=32, choices=CATEGORIES, default=FOOD)
    description = models.TextField()
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name


class Offer(models.Model):
    AVAILABLE = 'available'
    UNAVAILABLE = 'unavailable'

    STATUS = (
        (AVAILABLE, AVAILABLE),
        (UNAVAILABLE, UNAVAILABLE)
    )

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    status = models.CharField(max_length=16, choices=STATUS, default=AVAILABLE)
    quantity = models.CharField(max_length=128)
    expiration_date = models.DateTimeField(null=True, blank=True)


class Demand(models.Model):
    REQUESTED = 'requested'
    ACCEPTED = 'accepted'
    DENIED = 'denied'

    STATUS = (
        (REQUESTED, REQUESTED),
        (ACCEPTED, ACCEPTED),
        (DENIED, DENIED)
    )
    petitioner = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE)
    status = models.CharField(max_length=16, choices=STATUS, default=REQUESTED)
