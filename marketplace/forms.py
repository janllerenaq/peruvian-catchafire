from django.forms import ModelForm

from .models import Product, Offer, Demand


class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = ('category', 'description', 'name')


class OfferForm(ModelForm):
    class Meta:
        model = Offer
        fields = ('product', 'quantity', 'expiration_date')

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        if user:
            self.fields['product'].queryset = Product.objects.filter(
                created_by=user
            )


class DemandForm(ModelForm):
    class Meta:
        model = Demand
        fields = ('status', 'offer', 'petitioner')
