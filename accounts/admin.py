from django.contrib import admin
from django.contrib.auth.admin import UserAdmin


from .models import User


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    list_display = ('username', 'email', 'user_type')
    fieldsets = (
        (None, {
            'fields': ('username', 'email', 'password', 'user_type', 'image')
        }),
    )
