from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from accounts.models import User

from .forms import ProductForm, OfferForm, DemandForm
from .models import Product, Offer, Demand


class ListProductsView(ListView):
    model = Product

    def get_queryset(self):
        qs = super(ListProductsView, self).get_queryset()
        return qs.filter(created_by=self.request.user)


list_products = login_required(ListProductsView.as_view())


class CreateProductView(CreateView):
    model = Product
    form_class = ProductForm

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.created_by = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('marketplace:products-list')


create_product = login_required(CreateProductView.as_view())


class ListOffersView(ListView):
    model = Offer
    template_name = 'offers/list.html'

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.request.user.user_type == User.DONOR_INSTITUTION:
            queryset = queryset.filter(product__created_by=self.request.user)

        return queryset


list_offers = login_required(ListOffersView.as_view())


class CreateOfferView(CreateView):
    model = Offer
    form_class = OfferForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'user': self.request.user
        })
        return kwargs

    def get_success_url(self):
        return reverse('marketplace:offers-list')


create_offer = login_required(CreateOfferView.as_view())


class OfferDetailView(DetailView):
    model = Offer
    template_name = 'offers/detail.html'

    def post(self, request, *args, **kwargs):
        related_offer = self.get_object()
        Demand.objects.create(
            petitioner=request.user,
            offer=related_offer,
        )
        messages.add_message(
            request,
            messages.INFO,
            'La demanda para este producto fue registrada'
        )
        return redirect(reverse('marketplace:offer-detail', args=(self.kwargs['pk'], )))

    def get_context_data(self, **kw):
        context = super().get_context_data(**kw)
        if self.request.user.user_type == User.ONG_MEMBER:
            context['is_already_demanded'] = Demand.objects.filter(
                petitioner=self.request.user,
                offer=self.object
            ).exists()
        else:
            context["demands"] = Demand.objects.filter(
                offer__product__created_by=self.request.user
            )
        return context


offer_detail = login_required(OfferDetailView.as_view())


class ListDemandView(ListView):
    model = Demand
    form_class = DemandForm

    def get_queryset(self):
        qs = super(ListDemandView, self).get_queryset()
        if self.request.user.user_type == User.DONOR_INSTITUTION:
            qs = qs.filter(offer__product__created_by=self.request.user)

        return qs


list_demand = login_required(ListDemandView.as_view())
